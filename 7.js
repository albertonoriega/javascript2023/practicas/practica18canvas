// Constante que apunta al lienzo ( canvas)
lienzo = document.querySelector('#canvas');
// constante para dibujar en el lienzo
contexto = canvas.getContext("2d");

// Cada iteración suma 50
for (let i = 0; i < 600; i = i + 50) {
    //color de fondo en rojo
    contexto.fillStyle = "red";
    // Rectangulo de 40x40 con Y = 5 y la X varia dependiendo de la iteración
    contexto.fillRect(5 + i, 5, 40, 40);
    contexto.fillStyle = "blue";
    contexto.fillRect(5 + i, 60, 40, 40);
    contexto.fillStyle = "yellow";
    contexto.fillRect(5 + i, 115, 40, 40);
    contexto.fillStyle = "green";
    contexto.fillRect(5 + i, 170, 40, 40)
}

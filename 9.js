// Constante que apunta al lienzo ( canvas)
const lienzo = document.querySelector('#canvas');

// constante para dibujar en el lienzo
const contexto = lienzo.getContext("2d");

/* circunferencia grande */
// Cambiamos el color de fondo a amarillo
contexto.fillStyle = "yellow";
// Dibujamos el circulo completo
contexto.beginPath();
contexto.arc(400, 400, 200, 0, Math.PI * 2);
contexto.fill();


/* Dibujamos la boca */
contexto.beginPath();
contexto.lineWidth = 25;
contexto.arc(400, 425, 125, 0, Math.PI);
contexto.stroke();

/* Dibujamos los ojos */
contexto.fillStyle = "black";

// ojo derecho
contexto.beginPath()
contexto.arc(475, 350, 15, 0, Math.PI * 2);
contexto.fill();

// ojo izquierdo
contexto.beginPath()
contexto.arc(325, 350, 15, 0, Math.PI * 2);
contexto.fill();

/* Dibujamos el iris */

//cambiamos el color de fondo a blanco
contexto.fillStyle = "white";

contexto.beginPath()
contexto.arc(475, 350, 5, 0, Math.PI * 2);
contexto.fill();

// ojo izquierdo
contexto.beginPath()
contexto.arc(325, 350, 5, 0, Math.PI * 2);
contexto.fill();

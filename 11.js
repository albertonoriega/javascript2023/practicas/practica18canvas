// Constante que apunta al lienzo ( canvas)
lienzo = document.querySelector('#canvas');
// constante para dibujar en el lienzo
contexto = canvas.getContext("2d");

contexto.beginPath();
contexto.moveTo(0, 10);
contexto.lineTo(100, 10);
contexto.lineTo(100, 100);
contexto.lineTo(200, 100);
contexto.lineTo(200, 10);
contexto.lineTo(300, 10);
contexto.lineTo(300, 100);
contexto.lineTo(400, 100);
contexto.lineTo(400, 10);
contexto.lineTo(600, 10);
contexto.stroke();
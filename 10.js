// Constante que apunta al lienzo ( canvas)
lienzo = document.querySelector('#canvas');
// constante para dibujar en el lienzo
contexto = canvas.getContext("2d");

//triángulo relleno
contexto.beginPath();
contexto.moveTo(100, 100);
contexto.lineTo(200, 100);
contexto.lineTo(100, 200);
// como es relleno no hace falta closePath()
contexto.fill();

//triángulo sin relleno

contexto.beginPath();
contexto.moveTo(250, 150);
contexto.lineTo(250, 250);
contexto.lineTo(150, 250);
// como es stroke hay que cerrarlo usando closePath
contexto.closePath();
contexto.stroke();
// Constante que apunta al lienzo ( canvas)
lienzo = document.querySelector('#canvas');
// constante para dibujar en el lienzo
contexto = canvas.getContext("2d");

// Tres rectángulos
contexto.fillRect(25, 25, 200, 200);
contexto.clearRect(45, 45, 160, 160);
contexto.strokeRect(60, 60, 130, 130);

//Dibujar un triángulo
contexto.beginPath();
contexto.moveTo(400, 400);
contexto.font = "24px serif";
contexto.fillText("(400,400)", 400, 400);
contexto.lineTo(450, 500);
contexto.fillText("(450,500)", 450, 500);
contexto.lineTo(350, 400);
contexto.fillText("(350,400)", 250, 400);
contexto.fill();

//Dibujar semicírculo
contexto.beginPath();
contexto.arc(100, 500, 30, 0, Math.PI / 180 * 180);
contexto.stroke();

// Circulo completo relleno
contexto.beginPath();
contexto.arc(160, 550, 30, 0, Math.PI / 180 * 360);
contexto.fill();


